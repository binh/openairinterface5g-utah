#!/bin/bash

MCC=208
MNC=10
MME_NODE_NAME="node-1"

EXPERIMENT=$(echo $HOSTNAME | awk -F'.' '{print $2"."$3"."$4"."$5}')
MME=$MME_NODE_NAME'.'$EXPERIMENT

echo "Build and run eNodeB/UE ..."
/usr/local/src/openairinterface5g/SCRIPTS/build_run_oaisim_enb_ue.sh 1 $MME $MCC $MNC


