#!/bin/bash

/usr/local/src/openairinterface5g/SCRIPTS/kill_enb.sh

if [ ! -f /var/log/BUILT_ENB ]; then
	echo "BUILDING oaisim ..."
	/usr/local/src/openairinterface5g/SCRIPTS/setup_enb.sh	
fi

CMAKE="/usr/local/src/openairinterface5g/cmake_targets"

cd $CMAKE
sudo -E tools/run_enb_ue_virt_s1 -c /usr/local/src/openairinterface5g/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_mme.seperated_enb.conf
