#!/bin/bash

if [ $# -lt 4 ]; then
	echo "Usage: <is MME/HSS running on a separate machine?> <MME's hostname> <MCC, eg, 208> <MNC, eg, 10>"
	exit 1
fi
CMAKE="/usr/local/src/openairinterface5g/cmake_targets"

MCC=$3
MNC=$4


replace_sgi_ip(){
    ENB_CONFIG="/usr/local/src/openairinterface5g/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_mme.seperated_enb.conf"
    MME_HOSTNAME=$1
    #MME_INFO=$(ssh -o "StrictHostKeyChecking no" $MME_HOSTNAME "/usr/local/src/openair-cn/SCRIPTS/get_interface_map.pl")
    #if [ "" != "$MME_INFO" ]; then
    #	sudo touch /var/log/BUILT_ENB
    #else
    #	exit 1
    #fi
    #MME_IP=$(echo $MME_INFO | grep "link-0" | awk '{print $12}')
    
    #Hardcode
    MME_IP="10.10.1.2"
    echo "[Config]: Replacing MME's IP ($MME_IP) in $ENB_CONFIG"
    sudo sed -i "s/mme_ip_address.*/mme_ip_address      = ( { ipv4       = \"$MME_IP\";/" $ENB_CONFIG


    S1_U_INTERFACE=$(/usr/local/src/openair-cn/SCRIPTS/get_interface_map.pl | grep "link-0" | awk '{print $3}')
    S1_U_IP=$(/usr/local/src/openair-cn/SCRIPTS/get_interface_map.pl | grep "link-0" | awk '{print $5}')

    echo "[Config]: Replacing S1_U INTERFACE ($S1_U_INTERFACE) in $ENB_CONFIG"
    sudo sed -i "s/ENB_INTERFACE_NAME_FOR_S1_MME.*/ENB_INTERFACE_NAME_FOR_S1_MME = \"$S1_U_INTERFACE\";/" $ENB_CONFIG
    sudo sed -i "s/ENB_INTERFACE_NAME_FOR_S1U.*/ENB_INTERFACE_NAME_FOR_S1U = \"$S1_U_INTERFACE\";/" $ENB_CONFIG


    echo "[Config]: Replacing S1_U IP ($S1_U_IP) in $ENB_CONFIG"
    sudo sed -i "s/ENB_IPV4_ADDRESS_FOR_S1_MME.*/ENB_IPV4_ADDRESS_FOR_S1_MME = \"$S1_U_IP\/24\";/" $ENB_CONFIG
    sudo sed -i "s/ENB_IPV4_ADDRESS_FOR_S1U.*/ENB_IPV4_ADDRESS_FOR_S1U = \"$S1_U_IP\/24\";/" $ENB_CONFIG
}

if [ $1 -eq 1 ]; then 
	echo "[Config]: ENodeB is running on a separate machine"
	ENB_CONFIG="/usr/local/src/openairinterface5g/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_mme.conf"
	replace_sgi_ip $2
fi

echo "[Config]: Replacing MCC and MNC ($MCC, $MNC) in $ENB_CONFIG"
sudo sed -i "s/mobile_country_code.*/mobile_country_code = \"$MCC\";/" $ENB_CONFIG
sudo sed -i "s/mobile_network_code.*/mobile_network_code = \"$MNC\";/" $ENB_CONFIG


/usr/local/src/openairinterface5g/SCRIPTS/kill_enb.sh

cd $CMAKE
#sudo ./build_oai -c -I -g --oaisim -x --install-system-files #with scope
#sudo ./build_oai -c --oaisim -x
#sudo ./build_oai --UE
#sudo cp /usr/local/src/openairinterface5g/targets/bin/oaisim.Rel10 /usr/local/src/openairinterface5g/oaisim.Rel10.withscope

#sudo ./build_oai -c -I -g --oaisim  --install-system-files #with scope
#sudo ./build_oai -C
#sudo ./build_oai --oaisim
#sudo ./build_oai --UE
#sudo cp /usr/local/src/openairinterface5g/oaisim.Rel10.withscope /usr/local/src/openairinterface5g/targets/bin/
#sudo ./build_oai -c -I -g --oaisim --install-system-files

#echo "[Run]: Run simulated eNB/UE ..."
#/usr/local/src/openairinterface5g/SCRIPTS/run_oaisim_enb_ue.sh $1
