#!/bin/bash

CMAKE="/usr/local/src/openairinterface5g/cmake_targets"

if [ ! -f /var/log/BUILT_ENB ]; then
	echo "BUILDING oaisim ..."
	/usr/local/src/openairinterface5g/SCRIPTS/setup_enb.sh	
fi


cd $CMAKE
sudo -E tools/run_enb_ue_virt_s1_with_scope -c /usr/local/src/openairinterface5g/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_mme.seperated_enb.conf
